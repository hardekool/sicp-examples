(ns sicp.ch2
  (:import (clojure.lang Numbers)))

(defn make-rat-obscure [a b]
  (Numbers/toRatio (rationalize (/ a b))))

(defn numer-obscure [a]
  (numerator a))

(defn denom-obscure [a]
  (denominator a))

(defn make-rat [a b]
  (let [g (sicp.core/gcd a b)]
    (list (/ a g) (/ b g))))

(defn numer [a]
  (first a))

(defn denom [a]
  (first (rest a)))

(defn add-rat [x y]
  (make-rat (+ (* (numer x) (denom y)) (* (numer y) (denom x)))
            (* (denom x) (denom y))))

(defn sub-rat [x y]
  (make-rat (- (* (numer x) (denom y)) (* (numer y) (denom x)))
            (* (denom x) (denom y))))

(defn mul-rat [x y]
  (make-rat (* (numer x) (numer y)) (* (denom x) (denom y))))

(defn div-rat [x y]
  (make-rat (* (numer x) (denom y)) (* (denom x) (numer y))))

(defn equal-rat? [x y]
  (= (* (numer x) (denom y)) (* (numer y) (denom x))))

(defn print-rat [x]
  (str (numer x) "/" (denom x)))

;cons alternative
(defn alt-cons [x y]
  (fn [m]
    (cond (= m 0) x
          (= m 1) y
          :else (println "Arg not 0 or 1" m))))

(defn car [z] (z 0))
(defn cdr [z] (z 1))

(defn length [l]
  (if (empty? l)
    0
    (inc (length (rest l)))))

(defn append [list1 list2]
  (if (empty? list1)
    list2
    (cons (first list1) (append (rest list1) list2))))

;;Excersize 2.2
(defn make-point [a b]
  "constructor for points where a point is a pair of coordinates on 2D plane"
  (list a b))

(defn x-point [a]
  "selector for getting x-coordinate from point"
  (first a))

(defn y-point [a]
  "selector for getting y-coordinate from point"
  (first (rest a)))

(defn make-segment [a b]
  "segment is defined by a start and end point, representing a line in a plane"
  (cons a b))

(defn start-segment [a]
  "gets the point at the start of the line segment"
  (first a))

(defn end-segment [a]
  "gets the point at the end of the line segment"
  (rest a))

(defn midpoint-segment [a]
  "returns a point representing the midpoint of the supplied segment"
  (let [start-point (start-segment a)
        end-point (end-segment a)]
    (make-point (sicp.core/avg (x-point start-point) (x-point end-point))
                (sicp.core/avg (y-point start-point) (y-point end-point)))))

; 2.17
(defn last-pair [a]
  (let [val (first a)]
    (if (empty? (rest a))
      val
      (recur (rest a)))))

;2.18
(defn reverser [a]
  (loop [reversed '()
         a a]
    (if (empty? a)
      reversed
      (recur (cons (first a) reversed) (rest a)))))

;2.24
(defn for-each [f xs]
  (let [rest-xs (rest xs)]
    (f (first xs))
    (if (not (empty? rest-xs))
      (for-each f rest-xs)
      true)))

;2.27
(defn deep-reverse [tree]
  (loop [tree tree
         result '()]
    (let [is-pair? (seq? (first tree))]
      (cond (empty? tree)
            result
            is-pair?
            (recur (rest tree)
                   (cons (deep-reverse (first tree)) result))
            :else (recur (rest tree)
                         (cons (first tree) result))))))

(defn scale-tree [tree scale]
  (cond (not (seq? tree)) (* tree scale)
        (empty? tree) '()
        :else
        (cons (scale-tree (first tree) scale) (scale-tree (rest tree) scale))))

(defn tree-map [tree f]
  (map (fn [sub-tree]
         (if (seq? sub-tree)
           (tree-map sub-tree f)
           (f sub-tree))) tree))

(defn scale-list [items factor]
  (if (empty? items)
    items
    (cons (* (first items) factor) (scale-list (rest items) factor))))

(defn my-map [f items]
  (if (empty? items)
    items
    (cons (f (first items)) (my-map f (rest items)))))

(defn count-leaves [x]
  (cond (not (seq? x)) 1
        (empty? x) 0
        :else
        (+ (count-leaves (first x)) (count-leaves (rest x)))))

(defn filt [pred coll]
  (loop [result '()
         coll coll]
    (cond (empty? coll) (reverse result)
          (pred (first coll)) (recur (cons (first coll) result) (rest coll))
          :else (recur result (rest coll)))))

(defn my-reduce-right [op init coll]
  (if (empty? coll)
    init
    (op (first coll)
        (my-reduce-right op init (rest coll)))))

(defn my-reduce-left [op init coll]
  (if (empty? coll)
    init
    (my-reduce-left op (op init (first coll)) (rest coll))))

;2.34
(defn tail-call-reduce [op init coll]
  (loop [red init
         coll (reverse coll)]
    (if (empty? coll)
      red
      (recur (op (first coll) red) (rest coll)))))

(defn my-horner-eval [x coefficient-seq]
  (my-reduce-right (fn [this-coeff higher-terms]
                      (+ (* higher-terms x) this-coeff))
                   0 coefficient-seq))

(defn horner-eval [x coefficient-seq]
  (reduce (fn [this-coeff higher-terms]
            (+ (* this-coeff x) higher-terms))
          0 (reverse coefficient-seq)))

;2.36
(defn accumulate-n [op init s]
  (if (empty? (first s))
    '()
    (cons (my-reduce-right op init (map first s))
          (accumulate-n op init (map rest s)))))

(defn reduce-n [op init s]
  (if (empty? (first s))
    '()
    (cons (rest (reduce op init (list (map first s))))
          (reduce-n op init (map rest s)))))

;2.37
(defn dot-product [v w]
  (reduce + 0 (map * v w)))

(defn transpose [m]
  (accumulate-n cons '() m))

(defn matrix-*-vector [m v]
  (map (fn [row] (dot-product row v)) m))

(defn matrix-*-matrix [m n]
  (let [tr (transpose n)]
    (map (fn [m-row]
           (matrix-*-vector tr m-row)) m)))

;; like range with inclusive high
(defn enum-interval [low high]
  (if (> low high)
    '()
    (cons low (enum-interval (inc low) high))))

(defn flat-map [proc coll]
  (reduce sicp.ch2/append '() (map proc coll)))

(defn my-remove [x coll]
  (filter (fn [i] (not (= i x))) coll))

(defn permutations [s]
  (if (empty? s)
    (list '())
    (mapcat
      (fn [x]
        (map
          (fn [p] (cons x p))
          (permutations (my-remove x s)))) s)))

;2.40
; 1 <= j < i <= n
(defn create-pair-seq [n]
  (mapcat (fn [i]
            (map (fn [j] (list i j)) (range 1 i))) (range 1 (inc n))))

(defn prime-sum [pair]
  (sicp.core/prime? (+ (first pair) (first (rest pair)))))

(defn make-pair-sum [pair]
  (let [a (first pair)
        b (first (rest pair))
        sum (+ a b)]
    (list a b sum)))

(defn prime-sum-pairs [n]
  (map make-pair-sum (filter prime-sum (create-pair-seq n))))

;2.41
(defn triple-sum [triple]
  (reduce + triple))

(defn create-triple-seq [n]
  (mapcat (fn [i]
            (mapcat (fn [j]
                      (map (fn [k]
                             (list i j k)) (range j i)))
                    (range 1 i))) (range 1 n)))

(defn make-triple-sum [triple]
  (let [a (first triple)
        b (first (rest triple))
        c (first (rest (rest triple)))]
    (list a b c (+ a b c))))

(defn triple-sum-to-s [s n]
  (map make-triple-sum (filter (fn [triple]
                                 (= s (triple-sum triple))) (create-triple-seq n))))

;2.46
(defn make-vect [x y]
  (list x y))

(defn xcor-vect [v]
  (first v))

(defn ycor-vect [v]
  (first (rest v)))

(defn add-vec-any [& vs]
  (when (seq? vs)
    (apply map + vs)))

(defn add-vec [v1 v2]
  (make-vect (+ (xcor-vect v1) (xcor-vect v2)) (+ (ycor-vect v1) (ycor-vect v2))))

(defn sub-vec [v1 v2]
  (make-vect (- (xcor-vect v1) (xcor-vect v2)) (- (ycor-vect v1) (ycor-vect v2))))

(defn scale-vec [v s]
  (make-vect (* s (xcor-vect v)) (* s (ycor-vect v))))

;2.47
(defn make-frame [origin edge1 edge2]
  (list origin edge1 edge2))

(defn frame-origin [frame]
  (first frame))

(defn frame-edge1 [frame]
  (first (rest frame)))

(defn frame-edge2 [frame]
  (first (rest (rest frame))))

;2.48
(defn make-vec-segment [v1 v2]
  "creates directed line segment in plane from 2 vectors"
  (list v1 v2))

(defn start-vec-segment [segment]
  (first segment))

(defn end-vec-segment [segment]
  (first (rest segment)))

(defn frame-coord-map [frame]
  (fn [v]
    (add-vec (frame-origin frame)
             (add-vec (scale-vec (frame-edge1 frame) (xcor-vect v))
                      (scale-vec (frame-edge2 frame) (ycor-vect v))))))

(defn memq [item x]
  (cond (empty? x) false
        (= item (first x)) x
        :else (recur item (rest x))))

;symbolic diff example

(defn variable? [var]
  (symbol? var))

(defn same-variable? [a b]
  (and (variable? a) (variable? b) (= a b)))

(defn sum? [exp]
  (and (seq exp) (= (first exp) '+)))

(defn =number? [exp num]
  (and (number? exp) (= exp num)))

(defn make-sum [a b]
  (cond (=number? a 0) b
        (=number? b 0) a
        (and (number? a) (number? b)) (+ a b)
        :else (list '+ a b)))

(defn addend [sum]
  (first (rest sum)))

(defn augend [sum]
  (first (rest (rest sum))))

(defn product? [exp]
  (and (seq? exp) (= (first exp) '*)))

(defn make-product [a b]
  (cond (or (=number? a 0) (=number? b 0)) 0
        (=number? a 1) b
        (=number? b 1) a
        (and (number? a) (number? b)) (* a b)
        :else (list '* a b)))

(defn multiplier [prod]
  (first (rest prod)))

(defn multiplicand [prod]
  (first (rest (rest prod))))

(defn base [exp]
  (first (rest exp)))

(defn exponent [exp]
  (first (rest (rest exp))))

(defn exponentiation? [exp]
  (and (seq? exp) (= '** (first exp))))

(defn make-exponentiation [base exponent]
  (list '** base exponent))

(defn deriv-2 [exp var]
  (cond (number? exp) 0
        (variable? exp) (if (same-variable? exp var) 1 0)
        (sum? exp) (make-sum (sicp.core/deriv (addend exp) var)
                             (sicp.core/deriv (augend exp) var))
        (product? exp) (make-sum
                         (make-product (multiplier exp) (sicp.core/deriv (multiplicand exp) var))
                         (make-product (multiplicand exp) (sicp.core/deriv (multiplier exp) var)))
        (exponentiation? exp) (make-product (make-product (exponent exp) (make-exponentiation
                                                             (base exp) (make-sum (exponent exp) '-1)))
                                            (sicp.core/deriv (base exp) var))
        :else (println "Unknown expression for deriv" exp)))

;The sets worked example
(defn element-of-set? [x set]
  (cond (empty? set) false
        (= x (first set)) true
        :else (element-of-set? x (rest set))))

(defn adjoin-set [x set]
  (if (element-of-set? x set)
    set
    (cons x set)))

(defn intersection-set [a b]
  (cond (or (empty? a) (empty? b)) '()
        (element-of-set? (first a) b) (cons (first a) (intersection-set (rest a) b))
        :else (intersection-set (rest a) b)))

(defn union-set [a b]
  (cond (empty? a) b
        (element-of-set? (first a) b) (union-set (rest a) b)
        :else (cons (first a) (union-set (rest a) b))))

(defn intersection-ordered-set [a b]
  (if (or (empty? a) (empty? b)) '()
        (let [x1 (first a)
              x2 (first b)]
          (cond (= x1 x2)
                (cons x1 (intersection-ordered-set (rest a) (rest b)))
                (< x1 x2)
                (intersection-ordered-set (rest a) b)
                :else (intersection-ordered-set a (rest b))))))

(defn adjoin-ordered-set [x set]
  (cond (empty? set) (list x)
        (= x (first set)) set
        (< x (first set)) (cons x set)
        :else (cons (first set) (adjoin-ordered-set x (rest set)))))

;sets as trees
;tree is a list with 3 entries, (node, left-tree, right-tree)
(defn get-node [tree]
  (first tree))

(defn get-left-branch [tree]
  (first (rest tree)))

(defn get-right-branch [tree]
  (first (rest (rest tree))))

(defn create-tree-node [node left-tree right-tree]
  (list node left-tree right-tree))

(defn element-of-set-tree? [x set]
  (cond (empty? set) false
        (= x (get-node set)) true
        (< x (get-node set)) (element-of-set-tree? x (get-left-branch set))
        (> x (get-node set)) (element-of-set-tree? x (get-right-branch set))
        :else false))

(defn adjoin-set-tree [x set]
  (cond (empty? set) (create-tree-node x (list) (list))
        (> x (get-node set)) (create-tree-node (get-node set) (get-left-branch set) (adjoin-set-tree x (get-right-branch set)))
        (< x (get-node set)) (create-tree-node (get-node set) (adjoin-set-tree x (get-left-branch set)) (get-right-branch set))
        (= x (get-node set)) set))

(defn tree->list-1 [tree]
  (if (empty? tree)
    '()
    (concat (tree->list-1 (get-left-branch tree))
            (cons (get-node tree)
                  (tree->list-1 (get-right-branch tree))))))

(defn tree->list-2 [tree]
  (fn copy-to-list [tree result]
    (if (empty? tree)
      result
      (copy-to-list (get-left-branch tree)
                    (cons (get-node tree)
                          (copy-to-list (get-right-branch tree) result))))) '())

(defn partial-tree [elts n]
  (if (= 0 n)
    (cons '() elts)
    (let [left-size (quot (dec n) 2)]
      (let [left-result (partial-tree elts left-size)]
        (let [left-tree (first left-result)
              non-left-elts (rest left-result)
              right-size (- n (inc left-size))]
          (let [this-entry (first non-left-elts)
                right-result (partial-tree
                              (rest non-left-elts) right-size)]
            (let [right-tree (first right-result)
                  remaining-elts (rest right-result)]
              (cons
               (create-tree-node this-entry left-tree right-tree)
               remaining-elts))))))))

(defn list->tree [elements]
  (first (partial-tree elements (count elements))))
