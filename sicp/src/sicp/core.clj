(ns sicp.core)

;; this file contains the examples from chapter 1

(defn foo
  "I don't do a whole lot."
  [x]
  (println x "Hello, World!"))

; Prime factors of 13195 are 5 7 13 29
; what is the largest prime factor of 600851475143

(def threshold 0.001)

(defn avg [a b] (/ (+ a b) 2))

(defn next-geuss-square [geuss x] 
  (avg geuss (/ x geuss)))

(defn square [x] (* x x))

(defn next-geuss-cube [geuss x]
  (/ (+ (* 2 geuss) (/ x (square geuss))) 3))

(defn cube [x] (* (square x) x))

(defn abs [x] (max x (- x)))

(defn good-enough-cube? [geuss y]
  (< (abs (- y (cube geuss))) threshold))

(defn good-enough-square? [geuss x]
  (< (abs (- x (square geuss))) threshold))

(defn square-root [geuss x]
  (loop [a-geuss geuss 
         a-x x] 
    (if (good-enough-square? a-geuss a-x) 
      a-geuss 
      (recur (next-geuss-square a-geuss a-x) a-x))))

(defn cube-root [geuss x]
  (loop [a-geuss geuss
         a-x x]
    (if (good-enough-cube? a-geuss a-x)
      a-geuss
      (recur (next-geuss-cube a-geuss a-x) a-x))))

(defn fact-linear [x] 
  (loop [limit x
         cnt 1
         product 1]
    (if (> cnt limit)
      product
      (recur limit (inc cnt) (* cnt product)))))

(defn fact-recur [x]
  (loop [a-x x
         product 1]
    (if (<= a-x 0)
      product
      (recur (dec a-x) (* product a-x)))))

(defn ackermann [x y]
  (cond (= y 0) 0
        (= x 0) (* 2 y)
        (= y 1) 2
        true (ackermann (- x 1) (ackermann x (- y 1)))))

(defn acker [m n]
  (cond (= m 0) (inc n)
        (and (> m 0) (= n 0)) (acker (dec m) 1)
        (and (> m 0) (> n 0)) (acker (dec m) (acker m (dec n)))))

(defn fib-iter [n]
  (loop [a 1
         b 0
         cnt n]
    (if (= cnt 0)
      b
      (recur (+ a b) a (dec cnt)))))

(defn fib-recur [n]
  (cond (= n 0) 0
        (= n 1) 1
        :else (trampoline + (fib-recur (dec n)) (fib-recur (- n 2)))))

(def kinds-of-coin {1 1 2 5 3 10 4 25 5 50})

(defn cc [amount num-coin-types]
  (cond (= amount 0) 1
        (or (< amount 0) (= num-coin-types 0)) 0
        :else (+ (cc amount (dec num-coin-types))
                 (cc (- amount 
                        (kinds-of-coin num-coin-types)) num-coin-types))))

(defn trig-ident [x] 
  (- (* 3 x) (* 4 (cube x))))

(defn sine-approx [angle] 
  (if (< (abs angle) 0.1) 
    angle 
    (trampoline trig-ident (sine-approx (/ angle 3.0)))))

(defn exp [b n] 
    (if (= n 0) 
      1 
      (* b (exp b (dec n)))))

(defn iter-exp [b n]
  (loop [b b
         n n
         product 1]
    (if (= n 0)
      product
      (recur b (dec n) (* b product)))))

(defn  mult [a b]
  (if (= b 0)
    0
    (+ a (mult a (dec b)))))

;; a <- b
;; b <- mod a b
(defn gcd [a b]
  (loop [a a
         b b
         cnt 0]
    (if (= b 0)
      (do
        (println (str "number of loops: " cnt))
        a)
      (recur b (mod a b) (inc cnt)))))

(defn divides? [a b]
  (= 0 (mod a b)))

(defn find-divisor [n test-divisor]
  (cond (> (square test-divisor) n) n
        (divides? n test-divisor) test-divisor
        :else (recur n (inc test-divisor))))

(defn prime? [a] 
  (= a (find-divisor a 2)))

;who knows?
(defn find-largest-prime [limit]
  (if (prime? limit)
      limit
      (recur (dec limit))))

(#(mod (iter-exp (rand-int %) %) %) 7)

;; base^exponent mod m
(defn expmod [base exponent m]
  (cond (= exponent 0) 1
        (even? exponent) (mod (square (expmod base (/ exponent 2) m)) m)
        :else (mod (* base (expmod base (dec exponent) m)) m)))

(defn fermat-test? [n]
  (fn [a]
    (= (expmod a n n) a)
   (recur (inc (rand-int (dec n))))))

(defn quick-prime [n times]
    (cond (= 0 times) true
          (fermat-test? n) (quick-prime n (dec times))
          :else false))

(defn sum [term next a b]
  (if (> a b)
    0
    (trampoline + (term a) 
       (sum term next (next a) b))))

; 1/1*3 + 1/5*7 + 1/9*11
(defn pi-term [a]
  (/ 1 (* a (+ 2 a))))

(defn pi-next [a]
  (+ a 4))

; integral between a b of function f
; [f(a + dx/2) + f(a + dx + dx/2) + f(a + 2dx + dx/2)...]dx
(defn integral [f a b dx]
  (* dx (sum f #(+ dx %) (+ a (/ dx 2.0)) b)))

; let-f(x, y) = x(1 + xy)^2 + y(1 - y) + (1 + xy)(1 - y)
(defn let-f [x y]
  (let [a (+ 1 (* x y))
        b (- 1 y)]
    (+ (* x (square a)) (* y b) (* a b))))

; finding roots of f(x)
; f(a) < 0 < f(b)
; x = (a + b) / 2
; if (f(x) < 0) then root between x and b
; else root between a and x
; repeat until interval < threshold where interval = b - a
(defn root-finder [f a b]
  (let [midpoint (avg a b)
        test-value (f midpoint)]
    (cond (< (abs (- a b)) threshold) midpoint
          (< test-value 0) (root-finder f midpoint b)
          (> test-value 0) (root-finder f a midpoint)
          :else midpoint)))

(defn function [x]
  (+ (Math/sin x) (Math/cos x)))

(defn safe-root-finder [f a b]
  (let [fa (f a)
        fb (f b)
        increasing? (< fa fb)]
    (cond (and (< fa 0) (> fb 0)) (root-finder f a b)
          (and (> fa 0) (< fb 0)) (root-finder f b a)
          :else (println (interpose " " (vector "no root for" fa fb))))))

(defn good-enough? [a b]
  (< (abs (- a b)) threshold))

(defn fixed-point [f x]
  (if (good-enough? x (f x))
    x
    (recur f (f x))))

(defn fixed-point-with-cnt [f x]
  (loop [cnt 0
         x x]
    (if (good-enough? x (f x))
      (println x "in" cnt)
      (recur (inc cnt) (f x)))))

(defn average-damp [f]
  (fn [x] (avg x (f x))))

(defn fixed-point-square-root [x]
  (fixed-point (average-damp (fn [y] (/ x y))) 1.))

(defn fixed-point-cube-root [x]
  (fixed-point (average-damp (fn [y] (/ x (square y)))) 1.))

(def dx 0.000000001)

(defn deriv [g]
  (fn [x] (/ (- (g (+ x dx)) (g x)) dx)))

(defn newtons-method [g]
  (fn [x] (- x (/ (g x) ((deriv g) x)))))

(defn newton-fixed-point [g geuss]
  (fixed-point (newtons-method g) geuss))

(defn cubic [a b c]
  (fn [x]
    (+ (cube x) (* (square x) a) (* x b) c)))

(defn apply-n-time [f n]
  (loop [cnt n
         g f]
    (if (= 1 cnt)
      g
    (recur (dec cnt) g))))

(defn newton-square-root [x]
  (newton-fixed-point (fn [y] (- (square y) x)) 1.))

(defn iterative-improve [test? improve]
  (fn [geuss]
    (if (test? geuss)
      geuss
      (recur (improve geuss)))))

;;Excersize 1.46
(defn itr-imp-square-root [x]
  ((iterative-improve
     (fn [geuss] (good-enough-square? geuss x))
     (fn [geuss] (next-geuss-square geuss x))) x))

(defn itr-imp-fixed-point [f]
  (iterative-improve
    (fn [geuss] (good-enough? geuss (f geuss)))
    (fn [geuss] (f geuss))))

(defn -main [& args])
