(ns sicp.huffman)

(defn make-leaf [symbol weight]
  (list 'leaf symbol weight))

(defn is-leaf? [leaf]
  (= 'leaf (first leaf)))

(defn symbol-leaf [leaf]
  (first (rest leaf)))

(defn weight-leaf [leaf]
  (first (rest (rest leaf))))

(defn left-branch [tree]
  (first tree))

(defn right-branch [tree]
  (first (rest tree)))

(defn symbols [tree]
  (if (is-leaf? tree)
    (list (symbol-leaf tree))
    (first (rest (rest tree)))))

(defn weight [tree]
  (if (is-leaf? tree)
    (weight-leaf tree)
    (first (rest (rest (rest tree))))))

(defn make-code-tree [left right]
  (list left right (sicp.ch2/append (symbols left) (symbols right)) (+ (weight left) (weight right))))
