(ns sicp.joc)

(defn rand-ints [n]
  (take n (repeatedly #(rand-int n))))

(defn sort-parts [work]
  (lazy-seq
    (loop [[part & parts] work]
      (if-let [[pivot & xs] (seq? part)]
        (let [smaller? #(< % pivot)]
          (recur (list*
                   (filter smaller? xs)
                   pivot
                   (remove smaller? xs)
                   parts)))
        (when-let [[x & parts] parts]
          (cons x (sort-parts parts)))))))

(defn fnth [n]
  (apply comp (cons first (take (dec n) (repeat rest)))))

(defn join
  {:test (fn []
           (assert (= (join "," [1 2 3]) "1,2,3")))}
  [sep s]
  (apply str (interpose sep s)))

(def plays [{:band "Burial" :plays 979 :loved 9}
            {:band "Eno" :plays 2333 :loved 15}
            {:band "Bill Evans" :plays 979 :loved 9}
            {:band "Magma" :plays 2665 :loved 31}])

(sort-by #(/ (:plays %) (:loved %)) plays)

(defn column_pojection [column-names]
  (fn [row]
    (vec (map row column-names))))

(map (column_pojection [:band :plays :loved]) plays)